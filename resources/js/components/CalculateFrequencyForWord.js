import React, { useEffect, useState } from "react";

import LinearProgress from "@material-ui/core/LinearProgress";
import Paper from "@material-ui/core/Paper";
import { TextField } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  container: {
    padding: 10
  }
}));

let timeout = null;

const CalculateFrequencyForWord = ({ className, text }) => {
  const classes = useStyles();
  const [pending, setPending] = useState(true);
  const [word, setWord] = useState("Ordina");
  const [frequency, setFrequency] = useState(false);

  const fetchCalculateFrequencyForWord = () => {
    axios
      .post("/api/calculate-frequency-for-word", { text, word })
      .then(response => {
        setPending(false);
        setFrequency(response.data);
      })
      .catch(() => {
        setPending(false);
        setFrequency(false);
      });
  };

  useEffect(() => {
    clearTimeout(timeout);
    setPending(true);
    timeout = setTimeout(fetchCalculateFrequencyForWord, 500);
  }, [text, word]);

  return (
    <Paper className={className}>
      <div className={classes.container}>
        <Typography variant="h5" component="h2">
          CalculateFrequencyForWord
        </Typography>
        <TextField
          label="Word"
          value={word}
          onChange={e => {
            setWord(e.target.value);
          }}
          fullWidth
        />
        {frequency !== false ? (
          <Typography variant="h6" component="span">
            Word Frequency: {frequency}
          </Typography>
        ) : (
          false
        )}
      </div>
      {pending ? <LinearProgress color="secondary" /> : false}
    </Paper>
  );
};

export default CalculateFrequencyForWord;
