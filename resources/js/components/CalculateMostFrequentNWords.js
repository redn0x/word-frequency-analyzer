import React, { useEffect, useState } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TextField
} from "@material-ui/core";

import LinearProgress from "@material-ui/core/LinearProgress";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  container: {
    padding: 10
  }
}));

let timeout = null;

const CalculateMostFrequentNWords = ({ className, text }) => {
  const classes = useStyles();
  const [pending, setPending] = useState(true);
  const [nWords, setNWords] = useState(5);
  const [mostFrequentNWords, setMostFrequentNWords] = useState(false);

  const fetchCalculateMostFrequentNWords = () => {
    axios
      .post("/api/calculate-most-frequent-n-words", { text, nWords })
      .then(response => {
        setPending(false);
        setMostFrequentNWords(response.data);
      })
      .catch(() => {
        setPending(false);
        setMostFrequentNWords(false);
      });
  };

  useEffect(() => {
    clearTimeout(timeout);
    setPending(true);
    timeout = setTimeout(fetchCalculateMostFrequentNWords, 500);
  }, [text, nWords]);

  return (
    <Paper className={className}>
      <div className={classes.container}>
        <Typography variant="h5" component="h2">
          CalculateMostFrequentNWords
        </Typography>
        <TextField
          label="nWords"
          value={nWords}
          type="number"
          onChange={e => {
            setNWords(e.target.value);
          }}
          fullWidth
        />
      </div>
      {mostFrequentNWords ? (
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Word</TableCell>
              <TableCell>Frequency</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {mostFrequentNWords.map(item => (
              <TableRow key={item[0]}>
                <TableCell>{item[0]}</TableCell>
                <TableCell>{item[1]}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      ) : (
        false
      )}

      {pending ? <LinearProgress color="secondary" /> : false}
    </Paper>
  );
};

export default CalculateMostFrequentNWords;
