import React, { useState } from "react";

import CalculateFrequencyForWord from "./CalculateFrequencyForWord";
import CalculateHighestFrequency from "./CalculateHighestFrequency";
import CalculateMostFrequentNWords from "./CalculateMostFrequentNWords";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import { TextField } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import sampleText from "./sampleText";

const useStyles = makeStyles(() => ({
  box: {
    margin: "10px 0"
  },
  container: {
    padding: 10
  }
}));

const Layout = () => {
  const classes = useStyles();
  const [text, setText] = useState(sampleText);

  return (
    <Container maxWidth="md">
      <Paper className={`${classes.box} ${classes.container}`}>
        <Typography variant="h3" component="h1">
          Word Frequency Analyzer
        </Typography>
        <TextField
          label="Text"
          value={text}
          onChange={e => {
            setText(e.target.value);
          }}
          multiline
          fullWidth
        />
      </Paper>
      <CalculateHighestFrequency className={classes.box} text={text} />
      <CalculateFrequencyForWord className={classes.box} text={text} />
      <CalculateMostFrequentNWords className={classes.box} text={text} />
    </Container>
  );
};

export default Layout;
