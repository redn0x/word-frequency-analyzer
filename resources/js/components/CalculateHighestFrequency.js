import React, { useEffect, useState } from "react";

import LinearProgress from "@material-ui/core/LinearProgress";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  container: {
    padding: 10
  }
}));

let timeout = null;

const CalculateHighestFrequency = ({ className, text }) => {
  const classes = useStyles();
  const [pending, setPending] = useState(true);
  const [highestFrequency, setHighestFrequency] = useState(false);

  const fetchCalculateHighestFrequency = () => {
    axios
      .post("/api/calculate-highest-frequency", { text })
      .then(response => {
        setPending(false);
        setHighestFrequency(response.data);
      })
      .catch(() => {
        setPending(false);
        setHighestFrequency(false);
      });
  };

  useEffect(() => {
    clearTimeout(timeout);
    setPending(true);
    timeout = setTimeout(fetchCalculateHighestFrequency, 500);
  }, [text]);

  return (
    <Paper className={className}>
      <div className={classes.container}>
        <Typography variant="h5" component="h2">
          CalculateHighestFrequency
        </Typography>
        {highestFrequency ? (
          <Typography variant="h6" component="span">
            Highest Frequency: {highestFrequency}
          </Typography>
        ) : (
          false
        )}
      </div>
      {pending ? <LinearProgress color="secondary" /> : false}
    </Paper>
  );
};

export default CalculateHighestFrequency;
