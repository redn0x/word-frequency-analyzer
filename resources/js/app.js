import CssBaseline from "@material-ui/core/CssBaseline";
import Layout from "./components/Layout";
import React from "react";
import ReactDOM from "react-dom";
import { ThemeProvider } from "@material-ui/core/styles";
import theme from "./theme";

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <CssBaseline />
    <Layout />
  </ThemeProvider>,
  document.querySelector("#app")
);
