<?php
use App\Http\Controllers\WordFrequencyApiController;
use Illuminate\Support\Facades\Route;

/*
 * |--------------------------------------------------------------------------
 * | Web Routes
 * |--------------------------------------------------------------------------
 * |
 * | Here is where you can register web routes for your application. These
 * | routes are loaded by the RouteServiceProvider within a group which
 * | contains the "web" middleware group. Now create something great!
 * |
 */

Route::get('/', function () {
    return view('app');
});

Route::post('/api/calculate-frequency-for-word', [
    WordFrequencyApiController::class,
    'calculateFrequencyForWord'
]);
Route::post('/api/calculate-highest-frequency', [
    WordFrequencyApiController::class,
    'calculateHighestFrequency'
]);
Route::post('/api/calculate-most-frequent-n-words', [
    WordFrequencyApiController::class,
    'calculateMostFrequentNWords'
]);

