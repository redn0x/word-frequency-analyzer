# Word Frequency Analyzer
## Demo
[https://cedira.biz:1258/](https://cedira.biz:1258/)

## About 
The Word Frequency Analyzer is a simple tool to count the words in a text, 
it is build using the Laravel Framework, with ReactJS for the frontend ui. 

The class WordFrequencyAnalyzer incorperates 3 methods
- CalculateHighestFrequency
- CalculateFrequencyForWord
- CalculateMostFrequentNWords

All methods are tested with Unit tests

### Important file locations

- app/WordFrequencyAnalyzer/WordFrequencyAnalyzer.php
- app/WordFrequencyAnalyzer/WordFrequencyAnalyzerInterface.php
- app/Http/Controllers/WordFrequencyApiController.php

- resources/js/app.js
- resources/js/components/Layout.js
- resources/js/components/CalculateHighestFrequency.js
- resources/js/components/CalculateFrequencyForWord.js
- resources/js/components/CalculateMostFrequentNWords.js

- tests/Unit/WordFrequencyAnalyzerTest.php

### Commands
To run from commandline:
> php artisan serve

To run tests from commandline:
> php artisan test

## Docker
This application is available as a docker image

Pull:
> docker pull redn0x/word-frequency-analyzer

Run on port 8080
> docker run -it --rm -p8080:80 redn0x/word-frequency-analyzer

Run tests
> docker run -it --rm redn0x/word-frequency-analyzer php artisan test