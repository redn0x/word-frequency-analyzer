FROM hitalos/laravel:latest

COPY . /var/www

HEALTHCHECK --interval=30s --start-period=30s CMD curl -f http://localhost/ || exit 1