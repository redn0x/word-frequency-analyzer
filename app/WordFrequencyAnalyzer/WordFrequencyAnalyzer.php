<?php
namespace App\WordFrequencyAnalyzer;

class WordFrequencyAnalyzer implements WordFrequencyAnalyzerInterface
{

    /**
     *
     * {@inheritdoc}
     * @see \App\WordFrequencyAnalyzer\WordFrequencyAnalyzerInterface::calculateHighestFrequency()
     */
    public function calculateHighestFrequency(String $text)
    {
        $text = $this->santizeText($text);
        if ($text == "")
            return false;

        $words = explode(' ', $text);
        $countedWords = array_count_values($words);
        rsort($countedWords, SORT_STRING);
        $highestFrequency = $countedWords[0];

        return $highestFrequency;
    }

    /**
     *
     * {@inheritdoc}
     * @see \App\WordFrequencyAnalyzer\WordFrequencyAnalyzerInterface::calculateFrequencyForWord()
     */
    public function calculateFrequencyForWord(String $text, String $word)
    {
        $text = $this->santizeText($text);
        $word = trim(strtolower($word));

        if ($text == "" || $word == "")
            return 0;

        $text = " $text ";
        $word = " $word ";
        $frequency = 0;
        $offset = 0;

        while (($strPos = strpos($text, $word, $offset)) !== false) {
            $offset = $strPos + 1;
            ++ $frequency;
        }

        return $frequency;
    }

    /**
     *
     * {@inheritdoc}
     * @see \App\WordFrequencyAnalyzer\WordFrequencyAnalyzerInterface::calculateMostFrequentNWords()
     */
    public function calculateMostFrequentNWords(String $text, int $nWords)
    {
        $text = $this->santizeText($text);
        if ($text == "" || $nWords <= 0)
            return [];

        $words = explode(' ', $text);
        $countedWords = array_count_values($words);

        foreach ($countedWords as $key => $value)
            $list[] = [
                $key,
                $value
            ];

        array_multisort(array_column($list, 1), SORT_DESC, array_column($list, 0), SORT_ASC, $list);

        return array_slice($list, 0, $nWords);
        ;
    }

    /**
     * Returns the input text with the following modifications
     * -changed to lowercase
     * -removes special characters
     * -removes doublespaces with single space
     * -trims whitespace from end and start
     *
     * @param String $text
     * @return string
     */
    private function santizeText(String $text)
    {
        $text = strtolower($text); // change text to lowercase
        $text = preg_replace('/\W/', ' ', $text); // change special characters to space
        $text = preg_replace('/\s+/', ' ', $text); // change double spaces to single space
        $text = trim($text); // remove whitespace from start and end
        return $text;
    }
}
