<?php
namespace App\WordFrequencyAnalyzer;

interface WordFrequencyAnalyzerInterface
{

    /**
     * returns the highest frequency in the text (several words might actually have this frequency)
     *
     * @param String $text
     *
     * @return int
     */
    public function calculateHighestFrequency(String $text);

    /**
     * Return the frequency of the specified word
     *
     * @param String $text
     * @param String $word
     *
     * @return int
     */
    public function calculateFrequencyForWord(String $text, String $word);

    /**
     * Returns a list of the most frequent $nWords in the input text,
     * all the words are returned in lower case.
     * If several words have the same frequency,
     * this method return them in ascendant alphabetical order
     *
     * @param String $text
     * @param
     *            int
     */
    public function calculateMostFrequentNWords(String $text, int $nWords);
}

