<?php
namespace App\Http\Controllers;

use App\WordFrequencyAnalyzer\WordFrequencyAnalyzer;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class WordFrequencyApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * returns the highest frequency in the text (several words might actually have this frequency)
     *
     * @param Request $request
     * @return int
     */
    public function calculateHighestFrequency(Request $request)
    {
        $request->validate([
            'text' => 'required|string'
        ]);

        $analyzer = new WordFrequencyAnalyzer();
        return $analyzer->calculateHighestFrequency($request->get("text"));
    }

    /**
     * Return the frequency of the specified word
     *
     * @param Request $request
     * @return int
     */
    public function calculateFrequencyForWord(Request $request)
    {
        $request->validate([
            'text' => 'required|string',
            'word' => 'required|string'
        ]);
        $analyzer = new WordFrequencyAnalyzer();
        return $analyzer->calculateFrequencyForWord($request->get("text"), $request->get("word"));
    }

    /**
     * Returns a list of the most frequent $nWords in the input text,
     * all the words are returned in lower case.
     * If several words have the same frequency,
     * this method return them in ascendant alphabetical order
     *
     * @param Request $request
     * @return array
     */
    public function calculateMostFrequentNWords(Request $request)
    {
        $request->validate([
            'text' => 'required|string',
            'nWords' => 'required|integer'
        ]);

        $analyzer = new WordFrequencyAnalyzer();
        return $analyzer->calculateMostFrequentNWords($request->get("text"), $request->get("nWords"));
    }
}
