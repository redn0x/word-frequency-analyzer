<?php
namespace Tests\Unit;

use App\WordFrequencyAnalyzer\WordFrequencyAnalyzer;
use PHPUnit\Framework\TestCase;

class WordFrequencyAnalyzerTest extends TestCase
{

    /**
     * Unit test for the WordFrequencyAnalyzer Class
     *
     * @return void
     */
    public function test_calculateHighestFrequency()
    {
        $analyzer = new WordFrequencyAnalyzer();
        $text = "Het woord met de hoogste frequentie in deze tekst is het woord HET met een frequentie van 3.";

        // text with text
        $highestFrequency = $analyzer->calculateHighestFrequency($text);
        $this->assertEquals(3, $highestFrequency);

        // text with empty text
        $highestFrequency = $analyzer->calculateHighestFrequency("");
        $this->assertEquals(0, $highestFrequency);

        // text with text that is just whitespace
        $highestFrequency = $analyzer->calculateHighestFrequency("  ");
        $this->assertEquals(0, $highestFrequency);

        // text with text that is just special characters
        $highestFrequency = $analyzer->calculateHighestFrequency(" ...'[]\;\;");
        $this->assertEquals(0, $highestFrequency);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_calculateFrequencyForWord()
    {
        $analyzer = new WordFrequencyAnalyzer();
        $text = "Het    woord WooRD. komt 2 keer voor in deze   tekst met vele woorden. ";

        // test for word in text
        $frequency = $analyzer->calculateFrequencyForWord($text, "WOORD");
        $this->assertEquals(2, $frequency);

        // test for word that is not in text
        $frequency = $analyzer->calculateFrequencyForWord($text, "lipsum");
        $this->assertEquals(0, $frequency);

        // test for word empty
        $frequency = $analyzer->calculateFrequencyForWord($text, "");
        $this->assertEquals(0, $frequency);

        // test for text empty
        $frequency = $analyzer->calculateFrequencyForWord("", "woord");
        $this->assertEquals(0, $frequency);

        // test for text and word empty
        $frequency = $analyzer->calculateFrequencyForWord("", "");
        $this->assertEquals(0, $frequency);

        // test for word just whitespace
        $frequency = $analyzer->calculateFrequencyForWord($text, "   ");
        $this->assertEquals(0, $frequency);

        // test for text just whitespace
        $frequency = $analyzer->calculateFrequencyForWord("   ", "woord");
        $this->assertEquals(0, $frequency);

        // test for text and word empty
        $frequency = $analyzer->calculateFrequencyForWord("   ", "   ");
        $this->assertEquals(0, $frequency);

        // test for text and word containing special characters
        $frequency = $analyzer->calculateFrequencyForWord($text, "woord.");
        $this->assertEquals(0, $frequency);

        // test for word containing white space on start and end, the function is supposed to trim the whitespace
        $frequency = $analyzer->calculateFrequencyForWord($text, "  woord    ");
        $this->assertEquals(2, $frequency);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_calculateMostFrequentNWords()
    {
        // test expected list for text should be sorted for frequency descending and word order ascending
        $text = "De JavaScript developers van Ordina zitten bij JSRoots. Deze club bestaat momenteel uit een groep van 30 specialisten die actief bij de klant en in teams bezig zijn met het ontwikkelen van de nieuwste technologiën. Twee belangrijke kenmerken van deze unit zijn kennisdeling en een hechte club.";
        $nWords = 10;
        $expectedList = [
            [
                "van",
                4
            ],
            [
                "de",
                3
            ],
            [
                "bij",
                2
            ],
            [
                "club",
                2
            ],
            [
                "deze",
                2
            ],
            [
                "een",
                2
            ],
            [
                "en",
                2
            ],
            [
                "zijn",
                2
            ],
            [
                "actief",
                1
            ],
            [
                "belangrijke",
                1
            ]
        ];
        $analyzer = new WordFrequencyAnalyzer();
        $list = $analyzer->calculateMostFrequentNWords($text, $nWords);
        $this->assertCount($nWords, $list);
        $this->assertEquals($expectedList, $list);

        // test expected list for 0 nWords
        $nWords = 0;
        $expectedList = [];
        $analyzer = new WordFrequencyAnalyzer();
        $list = $analyzer->calculateMostFrequentNWords($text, $nWords);
        $this->assertCount(0, $list);
        $this->assertEquals($expectedList, $list);

        // test expected list for negative nWords, should return empty list
        $nWords = - 1;
        $expectedList = [];
        $analyzer = new WordFrequencyAnalyzer();
        $list = $analyzer->calculateMostFrequentNWords($text, $nWords);
        $this->assertCount(0, $list);
        $this->assertEquals($expectedList, $list);

        // test expected list for with fewer words than nWords
        $text = "Korte tekst tekst";
        $nWords = 5;
        $expectedList = [
            [
                "tekst",
                2
            ],
            [
                "korte",
                1
            ]
        ];
        $analyzer = new WordFrequencyAnalyzer();
        $list = $analyzer->calculateMostFrequentNWords($text, $nWords);
        $this->assertCount(2, $list);
        $this->assertEquals($expectedList, $list);
    }
}
